'use client'
import React from 'react'
import CountiesInstance from '../CountiesInstance'
import { Container } from "@chakra-ui/react";
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'

const CountiesPage2 = () => {
  return (
  <Container>
    <CountiesInstance 
      title='Barbour'
      prop1='15414'
      prop2='West Virigina'
      prop3='$42,260'
      prop4='Leer'
      prop5='Myers Clinic'
      imgSrc='https://upload.wikimedia.org/wikipedia/commons/7/72/BarbourCountyCourthouse.jpg'
      mapSrc='https://www.google.com/maps/vt/data=e3OJPJ5FiiX2g5vyegRUKkfJXhK8Q6eRY7gr91eEvkUnrhIF2c4oM1a8noB8MwOquhcILAE5qqwruHGx4XtXlMtxpIkhOXdU8sLyhW1O79dhUqh8Q1yTnFCMxbvIv5mcNMaW2cEs4Rij6vHbQlIHbjly5zfkX3RXQbMglSFLHmAtJ5NfI4cCgbAvFMY2t4BPFAFmCy5RmbTqxH0jECwiANIJiEefw0sYOjsGhDjfUxWG71WXm7bmkp9PU-LVAaFJiIJxB09REnRw-JSw0gIJ7spMR9rtOxqE7D-_uapiBUXg9tvKA_wLoyXQ1i1S4RPYAzZFQRIw'></CountiesInstance>
      <Row>
        <Col className='ms-4'>
            <Card className="ms-auto me-auto mb-4 mt-4">
                    <Card.Header>Coal Mines:</Card.Header>
                    <Card.Body>
                          <Link href='../coal-mines/coal-mine-2'>
                            <Button className='w-100'>
                                Leer
                            </Button>
                        </Link>
                    </Card.Body>
            </Card>
        </Col>
        <Col className="me-4">
            <Card className="ms-auto me-auto mb-4 mt-4">
                <Card.Header>Healthcare Providers:</Card.Header>
                <Card.Body>
                    <Link href='../healthcare-providers/healthcare-provider-2'>
                        <Button className='w-100'>
                          Myers Clinic
                        </Button>
                    </Link>
                </Card.Body>
            </Card>
        </Col>
      </Row>
  </Container>
  )
}

export default CountiesPage2