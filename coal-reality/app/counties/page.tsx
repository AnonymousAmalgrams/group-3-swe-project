'use client'
import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';  
import CountiesCard from './CountiesCard'

const counties = () => {
  return (
    <Container>
      <Container className="container text-center mt-5 mb-4">
        <h1>Counties</h1>
        <h3>3 Counties</h3>
      </Container>
      <Container>  
        <Row>  
          <Col>
            <CountiesCard title='Raleigh' 
              population='72882'
              state='West Virigina'
              median_income='$43,150'
              coal_mines='Affinity'
              coal_mine_link='/coal-mines/coal-mine-1'
              healthcare_providers='Raleigh General Hospital'
              healthcare_provider_link='/healthcare-providers/heathcare-provider-1'
              imgSrc='https://wvexplorer.com/wp-content/uploads/2020/10/Raleigh-County-West-Virginia-WV.jpg'
              counties_link='/counties/counties-1'></CountiesCard>
          </Col>  
          <Col>
            <CountiesCard title='Barbour' 
                population='15414'
                state='West Virigina'
                median_income='$42,260'
                coal_mines='Leer'
                coal_mine_link='/coal-mines/coal-mine-2'
                healthcare_providers=' Myers Clinic'
                healthcare_provider_link='/healthcare-providers/heathcare-provider-2'
                imgSrc='https://upload.wikimedia.org/wikipedia/commons/7/72/BarbourCountyCourthouse.jpg'
                counties_link='/counties/counties-2'></CountiesCard>
          </Col>  
          <Col>
            <CountiesCard title='Pike' 
                population='27005'
                state='Ohio'
                median_income='$44,961'
                coal_mines='Beckley Pocahontas Mine'
                coal_mine_link='/coal-mines/coal-mine-3'
                healthcare_providers='Valley View'
                healthcare_provider_link='/healthcare-providers/heathcare-provider-3'
                imgSrc='https://upload.wikimedia.org/wikipedia/commons/5/5e/Pike_County_Courthouse_in_Waverly.jpg'
                counties_link='/counties/counties-3'></CountiesCard>
          </Col>  
        </Row>  
      </Container>  
    </Container>
  )
}

export default counties