'use client'
import React from 'react'
import CountiesInstance from '../CountiesInstance'
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'

const CountiesPage3 = () => {
  return (
    <Container>     
      <CountiesInstance 
      title='Pike'
      prop1='27005'
      prop2='Ohio'
      prop3='$44,961'
      prop4='N/A'
      prop5='Valley View'
      imgSrc='https://upload.wikimedia.org/wikipedia/commons/5/5e/Pike_County_Courthouse_in_Waverly.jpg'
      mapSrc='https://www.google.com/maps/vt/data=OmIiP1Zy2I09IWICBtHLv-F5JOLlnT2fKiVoQl6zirBhkoLR6r_BD8a0CeWPN6fsNNiCPNrm4zuZ6cIRtbvyQF1X7ZGrg3RypVb8_UBzsdAtQ8b4l62z3XahAlqQChVg13Yo4wwYFBZz8KOeFPFMSSxuKuB3SjNXJiXpXdGx94OLOiA7pc49_Kr60wxWI3l9LkRfaYRtN0w4XraS0hjd9Txo9_vaE3rwlNRrbLwiFbliWOykGj2vKr7eDSH0siFi'></CountiesInstance>
      <Row>
        <Col className='ms-4'>
            <Card className="ms-auto me-auto mb-4 mt-4">
                    <Card.Header>Coal Mines:</Card.Header>
                    <Card.Body>
                      <Link href='../coal-mines/coal-mine-3'>
                          <Button className='w-100'>
                            Beckley Pocahontas
                          </Button>
                      </Link>
                    </Card.Body>
            </Card>
        </Col>
        <Col className="me-4">
            <Card className="ms-auto me-auto mb-4 mt-4">
                <Card.Header>Healthcare Providers</Card.Header>
                <Card.Body>
                    <Link href='../healthcare-providers/healthcare-provider-3'>
                        <Button className='w-100'>
                          Valley View
                        </Button>
                    </Link>
                </Card.Body>
            </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default CountiesPage3