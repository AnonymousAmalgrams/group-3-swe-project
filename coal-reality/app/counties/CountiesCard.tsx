'use client'
import React from 'react'
import Card from 'react-bootstrap/Card' 
import Link from 'next/link'
import "./CountiesCard.css"
import { Button } from 'react-bootstrap'


const CountiesCard = ({title, 
                      population, 
                      state, 
                      median_income, 
                      coal_mines, 
                      coal_mine_link,
                      healthcare_providers, 
                      healthcare_provider_link,
                      imgSrc,
                      counties_link} : {
                      title: string; 
                      population: string;
                      state: string;
                      median_income: string;
                      coal_mines: string;
                      coal_mine_link: string;
                      healthcare_providers: string; 
                      healthcare_provider_link: string;
                      imgSrc: string;
                      counties_link: string}) => {

    return (
        <Card className="ms-auto me-auto mb-4 mt-4">
            <Card.Img variant='top' src={imgSrc} alt="Card image cap" />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                    Population: {population}
                    <br/>
                    State: {state}
                    <br/>
                    Median Income: {median_income}
                    <br/>
                    Coal Mines: <Link href={coal_mine_link}>{coal_mines}</Link>
                    <br/>
                    Healthcare Providers: <Link href={healthcare_provider_link}>{healthcare_providers}</Link>
                    <br/>
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-center">
                <Link href={counties_link}>
                    <Button>Learn More</Button>
                </Link>
                <br/>
            </Card.Footer>
        </Card>
    )

}

export default CountiesCard