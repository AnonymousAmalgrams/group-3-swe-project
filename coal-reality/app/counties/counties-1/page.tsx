'use client'
import React from 'react'
import { Container } from "@chakra-ui/react";
import CountiesInstance from '../CountiesInstance'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'

const CountiesPage1 = () => {
  return (
    <Container>
      <CountiesInstance 
      title='Raleigh'
      prop1='72882'
      prop2='West Virigina'
      prop3='$43,150'
      prop4='Affinity, Beckley Pocahontas'
      prop5='Raleigh General Hospital'
      imgSrc='https://wvexplorer.com/wp-content/uploads/2020/10/Raleigh-County-West-Virginia-WV.jpg'
      mapSrc='https://www.google.com/maps/vt/data=8MYANFpm3j2G-RtiB87AwKdcgFunYjTDfuLakmcTKF4_vZSG1ZQQRK7Oc3Izy0-cYT37UsFUTE_kjydWich-xpzH3HemNDZ0YFP67DH28rJT19uBceDw5OievHsO0w0rNVlz_DjsdNvm9MBfSfjyG2R2_AspgiANZ44jfWyczlH9ERZwn9xWTiqvI24yh75WI-kEAQztrl39WLELy8JFj1FRz66g5vam4Io86ZLvEx6tELWpKGw2q3RMZ0wVaIRf'></CountiesInstance>

      <Row>
            <Col className='ms-4'>
                <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>Coal Mines:</Card.Header>
                        <Card.Body>
                            <Link href='../coal-mines/coal-mine-1'>
                                <Button className='w-100 mb-4'>
                                    Affinity
                                </Button>
                            </Link>
                        </Card.Body>
                </Card>
            </Col>
            <Col className="me-4">
                <Card className="ms-auto me-auto mb-4 mt-4">
                    <Card.Header>Healthcare Providers</Card.Header>
                    <Card.Body>
                        <Link href='../healthcare-providers/healthcare-provider-1'>
                            <Button className='w-100'>
                                Raleigh General Hospital
                            </Button>
                        </Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
  )
}

export default CountiesPage1