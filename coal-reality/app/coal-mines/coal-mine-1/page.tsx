'use client'
import React from 'react'
import { Container } from "@chakra-ui/react";
import CoalMineInstance from '../CoalMineInstance'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'


const CoalMinePage1 = () => {
  return (
    <Container>
      <CoalMineInstance 
      title='Affinity Mine'
      prop1='West Virginia (Southern)'
      prop2='Active'
      prop3='Underground'
      prop4='865,618'
      prop5='Appalachia Central'
      imgSrc='https://www.coalcampusa.com/sowv/gulf/affinity/newaffinity.jpg'
      mapSrc='https://utility.arcgisonline.com/arcgis/rest/directories/arcgisoutput/Utilities/PrintingTools_GPServer/x_____xh9JIJCFJQCF9IziGVPlMzg..x_____x_ags_b8fd69af-5cfe-11ee-a325-0a977670b8f5.jpg'></CoalMineInstance>

      <Row>
            <Col className='ms-4'>
                <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>County:</Card.Header>
                        <Card.Body>
                            <Link href='../counties/counties-1'>
                                <Button className='w-100 mb-4'>
                                    Raleigh
                                </Button>
                            </Link>
                        </Card.Body>
                </Card>
            </Col>
            <Col className="me-4">
                <Card className="ms-auto me-auto mb-4 mt-4">
                    <Card.Header>Healthcare Providers:</Card.Header>
                    <Card.Body>
                        <Link href='../healthcare-providers/healthcare-provider-1'>
                            <Button className='w-100'>
                                Raleigh General Hospital
                            </Button>
                        </Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
    // <div>
    // <div style={{ position: 'relative' }}>
    //     <img
    //       src="https://www.coalcampusa.com/sowv/gulf/affinity/newaffinity.jpg"
    //       alt="Affinity Mine"
    //       style={{
    //         width: '100%',
    //         height: 'auto',
    //         display: 'block',
    //       }}
    //     />
    //     <h1
    //       style={{
    //         position: 'absolute',
    //         top: '50%',
    //         left: '50%',
    //         transform: 'translate(-50%, -50%)',
    //         color: 'white',
    //         textShadow: '2px 2px 4px #000',
    //       }}
    //     >
    //       Affinity Mine
    //     </h1>
    //   </div>
    //   <h2>Mine Information:</h2>
    //   <p><strong>Name:</strong> Affinity Mine</p>
    //   <p><strong>Location:</strong> West Virginia (Southern)</p>
    //   <p><strong>Status:</strong> Active</p>
    //   <p><strong>Type:</strong> Underground</p>
    //   <p><strong>Total Production:</strong> 865,618</p>
    //   <p><strong>Region:</strong> Appalachia Central</p>

    //   <img src="https://utility.arcgisonline.com/arcgis/rest/directories/arcgisoutput/Utilities/PrintingTools_GPServer/x_____xh9JIJCFJQCF9IziGVPlMzg..x_____x_ags_b8fd69af-5cfe-11ee-a325-0a977670b8f5.jpg"></img>
    // </div>
  );
};

export default CoalMinePage1;
