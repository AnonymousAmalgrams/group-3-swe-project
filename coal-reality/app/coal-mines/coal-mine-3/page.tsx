'use client'
import React from 'react'
import { Container } from "@chakra-ui/react";
import CoalMineInstance from '../CoalMineInstance'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'



const CoalMinePage3 = () => {
  return (
    <Container>
      <CoalMineInstance 
      title='Beckley Pocahontas Mine'
      prop1='West Virginia (Southern)'
      prop2='Active'
      prop3='Underground'
      prop4='1,084,070'
      prop5='Appalachia Central'
      imgSrc='https://www.roadsideamerica.com/attract/images/va/VAPOCmine_chele.jpg'
      mapSrc='https://utility.arcgisonline.com/arcgis/rest/directories/arcgisoutput/Utilities/PrintingTools_GPServer/x_____xjJVsYF9wKk8CUe3f1fe3uw..x_____x_ags_27337b51-5d00-11ee-ba7e-0a409c6c0177.jpg'></CoalMineInstance>

      <Row>
            <Col className='ms-4'>
                <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>County:</Card.Header>
                        <Card.Body>
                            <Link href='../counties/counties-3'>
                                <Button className='w-100 mb-4'>
                                  Pike
                                </Button>
                            </Link>
                        </Card.Body>
                </Card>
            </Col>
            <Col className="me-4">
                <Card className="ms-auto me-auto mb-4 mt-4">
                    <Card.Header>Healthcare Providers:</Card.Header>
                    <Card.Body>
                        <Link href='../healthcare-providers/healthcare-provider-3'>
                            <Button className='w-100'>
                              Valley View
                            </Button>
                        </Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
    // <div>
    // <div style={{ position: 'relative' }}>
    //     <img
    //       src="https://www.roadsideamerica.com/attract/images/va/VAPOCmine_chele.jpg"
    //       alt="Leer Mine"
    //       style={{
    //         width: '100%',
    //         height: 'auto',
    //         display: 'block',
    //       }}
    //     />
    //     <h1
    //       style={{
    //         position: 'absolute',
    //         top: '50%',
    //         left: '50%',
    //         transform: 'translate(-50%, -50%)',
    //         color: 'white',
    //         textShadow: '2px 2px 4px #000',
    //       }}
    //     >
    //       Beckley Pocahontas Mine
    //     </h1>
    //   </div>
    //   <h2>Mine Information:</h2>
    //   <p><strong>Name:</strong> Beckley Pocahontas Mine</p>
    //   <p><strong>Location:</strong> West Virginia (Southern)</p>
    //   <p><strong>Status:</strong> Active</p>
    //   <p><strong>Type:</strong> Underground</p>
    //   <p><strong>Total Production:</strong> 1,084,070</p>
    //   <p><strong>Region:</strong> Appalachia Central</p>

    //   <img src="https://utility.arcgisonline.com/arcgis/rest/directories/arcgisoutput/Utilities/PrintingTools_GPServer/x_____xjJVsYF9wKk8CUe3f1fe3uw..x_____x_ags_27337b51-5d00-11ee-ba7e-0a409c6c0177.jpg"></img>
    // </div>
  );
};

export default CoalMinePage3;
