'use client'
import React from 'react'
import { Container } from "@chakra-ui/react";
import CoalMineInstance from '../CoalMineInstance'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'


const CoalMinePage2 = () => {
  return (
    <Container>
      <CoalMineInstance 
      title='Leer Mine'
      prop1='West Virginia (Northern)'
      prop2='Active'
      prop3='Underground'
      prop4='4,370,790'
      prop5='Appalachia Northern'
      imgSrc='https://www.coalage.com/wp-content/uploads/2015/05/2015_May2015_FEATURES_F1_1Grafton-267.jpg'
      mapSrc='https://utility.arcgisonline.com/arcgis/rest/directories/arcgisoutput/Utilities/PrintingTools_GPServer/x_____x92stdTK6GF-Q4w3yKeEggQ..x_____x_ags_e32b7385-5cff-11ee-b97a-0afd7775ca2d.jpg'></CoalMineInstance>

      <Row>
            <Col className='ms-4'>
                <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>County:</Card.Header>
                        <Card.Body>
                            <Link href='../counties/counties-2'>
                                <Button className='w-100 mb-4'>
                                  Barbour
                                </Button>
                            </Link>
                        </Card.Body>
                </Card>
            </Col>
            <Col className="me-4">
                <Card className="ms-auto me-auto mb-4 mt-4">
                    <Card.Header>Healthcare Providers:</Card.Header>
                    <Card.Body>
                        <Link href='../healthcare-providers/healthcare-provider-2'>
                            <Button className='w-100'>
                              Meyers Clinic
                            </Button>
                        </Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
    // <div>
    // <div style={{ position: 'relative' }}>
    //     <img
    //       src="https://www.coalage.com/wp-content/uploads/2015/05/2015_May2015_FEATURES_F1_1Grafton-267.jpg"
    //       alt="Leer Mine"
    //       style={{
    //         width: '100%',
    //         height: 'auto',
    //         display: 'block',
    //       }}
    //     />
    //     <h1
    //       style={{
    //         position: 'absolute',
    //         top: '50%',
    //         left: '50%',
    //         transform: 'translate(-50%, -50%)',
    //         color: 'white',
    //         textShadow: '2px 2px 4px #000',
    //       }}
    //     >
    //       Leer Mine
    //     </h1>
    //   </div>
    //   <h2>Mine Information:</h2>
    //   <p><strong>Name:</strong> Leer Mine</p>
    //   <p><strong>Location:</strong> West Virginia (Northern)</p>
    //   <p><strong>Status:</strong> Active</p>
    //   <p><strong>Type:</strong> Underground</p>
    //   <p><strong>Total Production:</strong> 4,370,790</p>
    //   <p><strong>Region:</strong> Appalachia Northern</p>

    //   <img src="https://utility.arcgisonline.com/arcgis/rest/directories/arcgisoutput/Utilities/PrintingTools_GPServer/x_____x92stdTK6GF-Q4w3yKeEggQ..x_____x_ags_e32b7385-5cff-11ee-b97a-0afd7775ca2d.jpg"></img>
    // </div>
  );
};

export default CoalMinePage2;
