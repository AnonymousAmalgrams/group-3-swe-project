'use client'
import React from 'react'
import Card from 'react-bootstrap/Card' 
import Link from 'next/link'
import "./CoalMineCard.css"
import { Button } from 'react-bootstrap'


const CoalMineCard = ({title, 
                      prop1, 
                      prop2, 
                      prop3, 
                      prop4, 
                      prop5, 
                      imgSrc,
                      coal_mine_link
                    } : {
                      title: string; 
                      prop1: string; prop2: string; prop3: string; prop4: string; prop5: string; 
                      imgSrc: string;
                      coal_mine_link: string;
                    }) => {

    return (
        <Card className="ms-auto me-auto mb-4 mt-4">
            <Card.Img variant='top' src={imgSrc} alt="Card image cap" />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                    County: {prop1}
                    <br/>
                    Status: {prop2}
                    <br/>
                    Mining Method: {prop3}
                    <br/>
                    Total Production: {prop4}
                    <br/>
                    Basin: {prop5}
                    <br/>
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-center">
                <Link href={coal_mine_link}>
                    <Button>Learn More</Button>
                </Link>
                <br/>
            </Card.Footer>
        </Card>
    )

}

export default CoalMineCard