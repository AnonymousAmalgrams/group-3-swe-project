'use client'
import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';  
import CoalMineCard from './CoalMineCard'
const coalMines = () => {
  return (
    <Container>
      <Container className="container text-center mt-5 mb-4">
        <h1>Coal Mines</h1>
        <h3>3 Coal Mines</h3>
      </Container>
      <Container>  
        <Row>  
          <Col>
            <CoalMineCard title='Affinity Mine' 
              prop1='West Virginia (Southern)'
              prop2='Active'
              prop3='Underground'
              prop4='865,618'
              prop5='Appalachia Central'
              imgSrc='https://www.coalcampusa.com/sowv/gulf/affinity/newaffinity.jpg'
              coal_mine_link='/coal-mines/coal-mine-1'></CoalMineCard>
          </Col>  
          <Col>
            <CoalMineCard title='Leer Mine' 
                prop1='West Virginia (Northern)'
                prop2='Active'
                prop3='Underground'
                prop4='4,370,790'
                prop5='Appalacia Northern'
                imgSrc='https://www.coalage.com/wp-content/uploads/2015/05/2015_May2015_FEATURES_F1_1Grafton-267.jpg'
                coal_mine_link='/coal-mines/coal-mine-2'></CoalMineCard>
          </Col>  
          <Col>
            <CoalMineCard title='Beckley Pocahontas Mine' 
                prop1='West Virginia (Southern)'
                prop2='Active'
                prop3='Underground'
                prop4='1,084,070'
                prop5='Appalachia Central'
                imgSrc='https://www.roadsideamerica.com/attract/images/va/VAPOCmine_chele.jpg'
                coal_mine_link='/coal-mines/coal-mine-3'></CoalMineCard>
          </Col>  
        </Row>  
      </Container>  
    </Container>
  )
}

export default coalMines