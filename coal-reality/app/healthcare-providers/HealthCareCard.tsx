'use client'
import React from 'react'
import Card from 'react-bootstrap/Card' 
import Link from 'next/link'
import "./HealthCareCard.css"
import { Button } from 'react-bootstrap'


const HealthCareCard = ({title, 
                      county,
                      county_page_link,
                      address, 
                      telephone, 
                      specialties, 
                      medicine, 
                      imgSrc,
                      healthcare_provider_link} : {
                      title: string; 
                      county: string; county_page_link: string; address: string; telephone: string; specialties: string; medicine: string; 
                      imgSrc: string;
                      healthcare_provider_link: string}) => {

    return (
        <Card className="ms-auto me-auto mb-4 mt-4">
            <Card.Img variant='top' src={imgSrc} alt="Card image cap" />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                    County: <Link href={county_page_link}>{county}</Link>
                    <br/>
                    Physical Address: {address}
                    <br/>
                    Telephone Numbers: {telephone}
                    <br/>
                    Specialty Services: {specialties}
                    <br/>
                    Medicine: {medicine}
                    <br/>
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-center">
                <Link href={healthcare_provider_link}>
                    <Button>Learn More</Button>
                </Link>
                <br/>
            </Card.Footer>
        </Card>
    )

}

export default HealthCareCard