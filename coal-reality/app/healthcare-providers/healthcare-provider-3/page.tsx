'use client'
import React from 'react'
import { Container } from "@chakra-ui/react";
import HealthcareProvidersInstance from '../HealthcareProvidersInstance'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'

const HealthcareProvider3 = () => {
  return (
    <Container>
      <HealthcareProvidersInstance 
      title='Valley View'
      prop1='Pike'
      prop2='227 Valley View Drive Waverly, Ohio 45690'
      prop3='(740) 947-7726'
      prop4='Behavioral Health, Dental Services, Family Medicine'
      prop5='https://www.valleyviewhealth.org'
      imgSrc='https://static.wixstatic.com/media/4c6b98_312f728ef6604f3c8e7600e1ad1e75fe~mv2.jpg/v1/crop/x_0,y_0,w_813,h_424/fill/w_872,h_452,al_c,lg_1,q_85,enc_auto/Waverly%20Location.jpg'
      mapSrc='https://www.google.com/maps/vt/data=OmIiP1Zy2I09IWICBtHLv-F5JOLlnT2fKiVoQl6zirBhkoLR6r_BD8a0CeWPN6fsNNiCPNrm4zuZ6cIRtbvyQF1X7ZGrg3RypVb8_UBzsdAtQ8b4l62z3XahAlqQChVg13Yo4wwYFBZz8KOeFPFMSSxuKuB3SjNXJiXpXdGx94OLOiA7pc49_Kr60wxWI3l9LkRfaYRtN0w4XraS0hjd9Txo9_vaE3rwlNRrbLwiFbliWOykGj2vKr7eDSH0siFi'></HealthcareProvidersInstance>
      <Row>
            <Col className='ms-4'>
                <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>County:</Card.Header>
                        <Card.Body>
                            <Link href='../counties/counties-3'>
                                <Button className='w-100 mb-4'>
                                  Pike
                                </Button>
                            </Link>
                        </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
  )
}

export default HealthcareProvider3