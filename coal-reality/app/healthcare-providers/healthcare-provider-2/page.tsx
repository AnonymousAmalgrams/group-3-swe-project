'use client'
import React from 'react'
import { Container } from "@chakra-ui/react";
import HealthcareProvidersInstance from '../HealthcareProvidersInstance'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'

const HealthcareProvider2 = () => {
  return (
    <Container>
      <HealthcareProvidersInstance 
      title='Meyers Clinic'
      prop1='Barbour'
      prop2='116 McClellan Drive, Philippi, WV 26416'
      prop3='(304) 457-2800'
      prop4='Geriatric Care, Pediatric Care, Sports Physicals'
      prop5='https://barbourhealth.org/myers-clinic'
      imgSrc='https://barbourhealth.org/wp-content/uploads/2023/03/Myers-Clinic-Landing.jpg'
      mapSrc='https://www.google.com/maps/vt/data=e3OJPJ5FiiX2g5vyegRUKkfJXhK8Q6eRY7gr91eEvkUnrhIF2c4oM1a8noB8MwOquhcILAE5qqwruHGx4XtXlMtxpIkhOXdU8sLyhW1O79dhUqh8Q1yTnFCMxbvIv5mcNMaW2cEs4Rij6vHbQlIHbjly5zfkX3RXQbMglSFLHmAtJ5NfI4cCgbAvFMY2t4BPFAFmCy5RmbTqxH0jECwiANIJiEefw0sYOjsGhDjfUxWG71WXm7bmkp9PU-LVAaFJiIJxB09REnRw-JSw0gIJ7spMR9rtOxqE7D-_uapiBUXg9tvKA_wLoyXQ1i1S4RPYAzZFQRIw'></HealthcareProvidersInstance>
      <Row>
            <Col className='ms-4'>
                <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>County:</Card.Header>
                        <Card.Body>
                            <Link href='../counties/counties-2'>
                                <Button className='w-100 mb-4'>
                                  Barbour
                                </Button>
                            </Link>
                        </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
  )
}

export default HealthcareProvider2