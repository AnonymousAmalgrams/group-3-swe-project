'use client'
import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';  
import HealthCareCard from './HealthCareCard'

const healthcareProviders = () => {
  return (
    <Container>
    <Container className="container text-center mt-5 mb-4">
      <h1>Health Care Providers</h1>
      <h3>3 Health Care Providers</h3>
    </Container>
    <Container>  
      <Row>  
        <Col>
          <HealthCareCard title='Raleigh General Hospital' 
            county='Raleigh'
            county_page_link='/counties/counties-1'
            address='1710 Harper Road Beckley, WV 25801'
            telephone='(304)-256-4100'
            specialties='Heart Health, Orthopedics, Stroke Program, Surgery'
            medicine='https://www.raleighgeneral.com/'
            imgSrc='https://www.lootpress.com/wp-content/uploads/2021/04/2OVBGLAZVZDTXM2Q7L4NK6OYPU-1140x570.jpg'
            healthcare_provider_link='/healthcare-providers/healthcare-provider-1'></HealthCareCard>
        </Col>  
        <Col>
          <HealthCareCard title='Meyers Clinic' 
              county='Barbour'
              county_page_link='/counties/counties-2'
              address='116 McClellan Drive, Philippi, WV 26416'
              telephone='(304) 457-2800'
              specialties='Geriatric Care, Pediatric Care, Sports Physicals'
              medicine='https://barbourhealth.org/myers-clinic'
              imgSrc='https://barbourhealth.org/wp-content/uploads/2023/03/Myers-Clinic-Landing.jpg'
              healthcare_provider_link='/healthcare-providers/healthcare-provider-2'></HealthCareCard>
        </Col>  
        <Col>
          <HealthCareCard title='Valley View' 
              county='Pike'
              county_page_link='/counties/counties-3'
              address='227 Valley View Drive Waverly, Ohio 45690'
              telephone='(740) 947-7726'
              specialties='Behavioral Health, Dental Services, Family Medicine'
              medicine='https://www.valleyviewhealth.org'
              imgSrc='https://static.wixstatic.com/media/4c6b98_312f728ef6604f3c8e7600e1ad1e75fe~mv2.jpg/v1/crop/x_0,y_0,w_813,h_424/fill/w_872,h_452,al_c,lg_1,q_85,enc_auto/Waverly%20Location.jpg'
              healthcare_provider_link='/healthcare-providers/healthcare-provider-3'></HealthCareCard>
        </Col>  
      </Row>  
    </Container>  
  </Container>
  )
}

export default healthcareProviders