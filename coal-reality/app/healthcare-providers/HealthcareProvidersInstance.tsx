'use client'
import { Container } from "@chakra-ui/react";
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'

const HealthcareProvidersInstance = ({title, 
                            prop1, 
                            prop2, 
                            prop3, 
                            prop4, 
                            prop5, 
                            imgSrc,
                            mapSrc} : {
                            title: string; 
                            prop1: string; prop2: string; prop3: string; prop4: string; prop5: string; 
                            imgSrc: string;
                            mapSrc: string}) => {
    return (
        <Container>
            <Container style={{ position: 'relative' }}>
                <img
                src={imgSrc}
                alt={title}
                style={{
                    width: '100%',
                    height: 'auto',
                    display: 'block',
                }}
                />
                <h1
                style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    color: 'white',
                    textShadow: '2px 2px 4px #000',
                }}
                >
                {title}
                </h1>
            </Container>
            <Container>
                <Row className='mb-4'>
                    <Col className='ms-4 mb-4'>
                        <Card className="ms-auto me-auto mb-4 mt-4 h-100">
                            <Card.Header>Healthcare Provider Information:</Card.Header>
                            <Card.Body>
                                <strong>Name:</strong> {title}
                                <br/>
                                <strong>County:</strong> {prop1}
                                <br/>
                                <strong>Physical Address:</strong> {prop2}
                                <br/>
                                <strong>Telephone Numbers:</strong> {prop3}
                                <br/>
                                <strong>Specialty Services:</strong> {prop4}
                                <br/>
                                <strong>Website:</strong> {prop5}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="ms-auto me-4 mb-4 mt-4">
                        <img src={mapSrc} style={{width: '100%'}}></img>
                    </Col>
                </Row>
            </Container>   
        </Container>
    )
  }
  
  export default HealthcareProvidersInstance