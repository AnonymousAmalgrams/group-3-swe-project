'use client'
import React from 'react'
import { Container } from "@chakra-ui/react";
import HealthcareProvidersInstance from '../HealthcareProvidersInstance'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'

const HealthcareProvider1 = () => {
  return (
    <Container>
      <HealthcareProvidersInstance 
      title='Raleigh General Hospital'
      prop1='Raleigh'
      prop2='1710 Harper Road Beckley, WV 25801'
      prop3='(304)-256-4100'
      prop4='Heart Health, Orthopedics, Stroke Program, Surgery'
      prop5='https://www.raleighgeneral.com/'
      imgSrc='https://www.lootpress.com/wp-content/uploads/2021/04/2OVBGLAZVZDTXM2Q7L4NK6OYPU-1140x570.jpg'
      mapSrc='https://www.google.com/search?q=raleigh+general+hospital+west+virginia+map&rlz=1C1CHBF_enUS935US935&oq=raleigh+general+hospital+west+virginia+map&aqs=chrome..69i57j33i160.2979j1j4&sourceid=chrome&ie=UTF-8#'></HealthcareProvidersInstance>

      <Row>
            <Col className='ms-4'>
                <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>County:</Card.Header>
                        <Card.Body>
                            <Link href='../counties/counties-1'>
                                <Button className='w-100 mb-4'>
                                  Raleigh
                                </Button>
                            </Link>
                        </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
  )
}

export default HealthcareProvider1