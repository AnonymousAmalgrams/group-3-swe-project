import Link from 'next/link';
import styles from './Navbar.module.css';

const Navbar = () => {
  return (
    <nav className={styles.navbar}>
      <ul className={styles.navList}>
      <li className={styles.navItem}>
          <Link href="/">Home</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/about">About</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/coal-mines">Coal Mines</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/counties">Counties</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/healthcare-providers">Healthcare Providers</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
