import React from 'react'
import "./page.css";

export default function Home() {
  return (
    <div>
      <div className="bg-image">
          <h1 className="bg-text">This is the coal reality.</h1>
      </div>
      <br></br>
      <br></br>
      <div>
        <h1 className='centered-text'>Overview</h1>
        <p>
          The proposed project aims to address the complex relationship between coal mining, environmental impact, and public health in the Appalachian region through a comprehensive website. It will consist of three primary models: Counties, Coal Mines, and Healthcare Providers, each with a substantial number of instances and various attributes for filtering and sorting.
        </p>
        <br></br>
        <p>
          Counties (423 instances) will have attributes such as Population, State, Average Age, Average Income, and Employment Rates. This data will provide a contextual backdrop for understanding the socioeconomic and demographic conditions in the region.
        </p>
        <br></br>
        <p>
          Coal Mines (200 instances) will feature attributes like County, Status (Active/Inactive), Mining Method, Production, and Basin. These details will help users access critical information about specific mines, their operational status, and production methods.
        </p>
        <br></br>
        <p>
          Healthcare Providers (500 instances) will have attributes like Name, County, Physical Address, Telephone Number, Specialty Services, and Insurance Accepted. This information will be vital for individuals seeking healthcare support due to health issues resulting from coal mining activities. The website will integrate various media types for each model, including maps and images for Counties, maps of mine locations, mine images, and production data feeds for Coal Mines, and  provider information, building images, and location maps for Healthcare Providers.
        </p>
        <br></br>
        <p>
          The website will answer important questions related to coal mining and its impact, such as identifying coal mines in the Appalachian region, locating nearby healthcare providers for those affected by mining-related health issues, identifying affected counties in the region, and discovering which diseases are supported by specific healthcare organizations in Appalachia. This project will provide a valuable resource for residents and stakeholders seeking information on coal mining&apos;s effects on health and the environment, while also aiding individuals in finding appropriate healthcare support in affected areas.
        </p>
      </div>
    </div>
    
  )
}